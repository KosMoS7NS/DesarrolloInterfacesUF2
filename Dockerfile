#
# Build stage
#
FROM maven:3.8.5-openjdk-17-slim AS build
WORKDIR /home/app/
COPY . /home/app/
RUN mvn clean package -B -DskipTests --also-make
#
# Package stage
#
FROM openjdk:17-alpine
COPY --from=build /home/app/target/DesarrolloInterfacesUF2-1.0-SNAPSHOT.jar /usr/local/lib/DesarrolloInterfacesUF2-1.0-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar","/usr/local/lib/DesarrolloInterfacesUF2-1.0-SNAPSHOT.jar"]