package org.ifp;

public class Main {

    public static int resultadoDivision(int a, int b) {
        return a / b;
    }

    public static void main(String[] args) {
        int a = 10;
        int b = 2;

        System.out.printf("Resultado: %s", resultadoDivision(a, b));
    }
}