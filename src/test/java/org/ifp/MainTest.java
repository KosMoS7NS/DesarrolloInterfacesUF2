package org.ifp;


import org.junit.Assert;
import org.junit.Test;

public class MainTest {

    @Test
    public void resultadoDivision() {
        int resultado = Main.resultadoDivision(10, 2);
        Assert.assertEquals(5, resultado);
    }
}